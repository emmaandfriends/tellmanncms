<div class="container">
   <div class="row">
      <div class="col-sm-12 mt-4">
         <center><span class="display-4">Wählen Sie Ihre berufliche Situation</span><br/></center><br/>
      </div>
      <div class="col-sm-12">
            <div class="row">
               <div class="col-sm-4">
                  <a href="/jobwahl/selbststaendig"><div style="background: #eee; padding-top: 50px; padding-bottom: 50px;">
                     <center><p>Selbstständig</p></center>
                  </div></a>
               </div>
               <div class="col-sm-4">
                  <a href="/jobwahl/angestellter"><div style="background: #eee; padding-top: 50px; padding-bottom: 50px;">
                     <center><p>Angestellt</p></center>
                  </div></a>
               </div>
                <div class="col-sm-4">
                  <a href="/jobwahl/beamter"><div style="background: #eee; padding-top: 50px; padding-bottom: 50px;">
                     <center><p>Beamter</p></center>
                  </div></a>
               </div>
            </div>
      </div>
   </div>
</div>
