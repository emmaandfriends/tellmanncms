<div class="navbar">
    <div class="container">
        <div class="row" style="width: 100%;">
			
			<div class="col-sm-3">
				<img class="logo" src="{{ @themepath.'/images/logo.png'}}">	
			</div>
			
            <div class="col-sm-7"> 
				<nav class="main_navigation">
                	<ul class="list-inline" style="margin: 30px 0"> 
						<li class="list-inline-item"><a href="/di-altersvorsorge">DiAltersvorsorge</a></li>
					   	<li class="list-inline-item"><a href="/beratung">Beratung</a></li>
					   	<li class="list-inline-item"><a href="/di-altersvorsorge">Support</a></li>
					   	<li class="list-inline-item"><a href="/di-altersvorsorge">Account</a></li>
                	</ul>
				</nav>
				
				
				
            </div>
            <div class="col-sm-2">
				<ul class="list-inline icon-list" style="margin: 30px 0"> 
					<li class="list-inline-item"><img style="margin-top: 7px;" src="{{ @themepath.'/images/search_icon.png'}}"></li>
					<li class="list-inline-item"><img src="{{ @themepath.'/images/cart_icon.png'}}"></li>
					
					<!-- <li class="list-inline-item text-align-right float-right"><h3 class="display-4" style="font-size: 1.2em; margin-top: 4px">Hallo {{@username}}!!</h3></li> -->
					
				</ul>
            </div>
        </div>    
    </div>
</div>