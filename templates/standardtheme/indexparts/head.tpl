<title>Test</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>

<link href="{{ @themepath.'/css/theme.css'}}" rel="stylesheet">

<!-- STORE.JS -->
<script src="{{ @themepath.'/js/store.everything.min.js'}}"></script>

<!-- JS VIDEO PLAYER -->
<link href="http://vjs.zencdn.net/6.2.7/video-js.css" rel="stylesheet">
<script src="http://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script>
<script src="{{ @themepath.'/js/video.js'}}"></script>
<script src="{{ @themepath.'/js/videojs-resume.js'}}"></script>
<link href="{{ @themepath.'/css/videojs-resume.css'}}"/>

<script src="{{ @themepath.'/js/videojs-bug.min.js'}}"></script> 
<link href="{{ @themepath.'/css/videojs-bug.css'}}" rel="stylesheet">

<!-- RANGE SLIDER -->
<link href="{{ @themepath.'/css/nouislider.min.css'}}" rel="stylesheet">
<script src="{{ @themepath.'/js/nouislider.min.js'}}"></script>

<!-- DROPZONE -->
<script src="{{ @themepath.'/js/dropzone.js'}}"></script>

<!-- JS PROGRESS BAR -->
<link rel="stylesheet" type="text/css" href="{{ @themepath.'/css/loading-bar.css'}}"/>
<script type="text/javascript" src="{{ @themepath.'/js/loading-bar.js'}}"></script>

<!-- SWIPER SLIDER -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.0.7/css/swiper.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.0.7/js/swiper.min.js"></script>