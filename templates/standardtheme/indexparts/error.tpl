<div class="row">
    <div class="col-sm-12">
        <repeat group="{{ @ErrorMessages }}" value="{{ @ErrorMessage }}">
            <div class="row">
                <div class="col-sm-12">
                    <p>{{ @ErrorMessage.msg }} {{ @ErrorMessage.type }}</p>
                    
                </div>
            </div>
        </repeat>
    </div>
</div>