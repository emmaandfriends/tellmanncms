<html>
    <head>
		<include href="templates/standardtheme/indexparts/head.tpl" />
    </head>
    <body>
        <include href="templates/standardtheme/indexparts/menu.tpl" />
        <include href="templates/standardtheme/indexparts/error.tpl" />
        <include href="{{ 'templates/standardtheme/'.@contentfolder.'/index.tpl' }}"/>
    </body>
</html>
    