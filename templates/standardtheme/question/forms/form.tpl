<repeat group="{{ @forms }}" value="{{ @form }}">
    <div class="col-sm-12">
        <repeat group="{{@form['fields']}}" value ="{{ @field }}">
            <div class="row">
                <check if="{{@field['type'] == 'select' }}">
                    <true>
                        <select id="{{@field['id']}}">
                            <repeat group="{{@field['options']}}" value ="{{ @option }}">
                                <option value="{{ @option['value'] }}">{{ @option['label'] }}</option>
                            </repeat>
                        </select>
                    </true>
                    <false>
                        <label class="col-sm-2" for="{{@field['id']}}">{{@field['label']}}</label>
                        <input class="col-sm-10" id="{{@field['id']}}" type="{{@field['type']}}" value="{{@field['defaultValue']}}"/>
                    </false>
                </check>
            </div>
        </repeat>
        <input type="hidden" name="csrf" value="{{@CSRF}}" />
    </div>
</repeat>