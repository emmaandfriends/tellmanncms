<div class="container">
    <div class="row">
    <repeat group="{{ @forms }}" value="{{ @form }}">
            <repeat group="{{@form['fields'][0]['options']}}" value ="{{ @option }}">
                <div class="col-sm-3">
                    <div class="row">
                        <div class="person" style="width: 100%; margin: 10px;">
                            <h3 class="name">{{@option['label']}}</h3>
                            <img src="{{@option['image']}}"/>
                            <p class="name">{{@option['name']}}</p>
                            <p class="position">{{@option['position']}}</p>
                            <p class="info">{{@option['info']}}</p>
                            <p class="education">{{@option['education']}}</p>
                            <a href="{{@option['video']}}" class="video">Ich stelle mich vor</a>
                        </div>
                    </div>
                </div>
            </repeat>
            <input type="hidden" name="csrf" value="{{@CSRF}}" />
    </repeat>
    </div>
</div>



<!--
<repeat group="{{ @forms }}" value="{{ @form }}">
        <repeat group="{{@form['fields']}}" value ="{{ @field }}">
            <div class="col-sm-4">
            <div class="row">
                <check if="{{@field['type'] == 'select' }}">
                    <true>
                        <select id="{{@field['id']}}">
                            <repeat group="{{@field['options']}}" value ="{{ @option }}">
                                <option value="{{ @option['value'] }}">{{ @option['label'] }}</option>
                            </repeat>
                        </select>
                    </true>
                    <false>
                        <label class="col-sm-2" for="{{@field['id']}}">{{@field['label']}}</label>
                        <input class="col-sm-10" id="{{@field['id']}}" type="{{@field['type']}}" value="{{@field['defaultValue']}}"/>
                    </false>
                </check>
            </div>
            </div>
        </repeat>
        <input type="hidden" name="csrf" value="{{@CSRF}}" />
</repeat>
-->