<if check="{{ @textberatung == true}}">

    <true>
        
        <!--- Textberatung --->
        <div class="container beratung">
    
           <div class="row headbar">
                <div class="col-sm-12 text-center">
                    <h1>Deine Altersvorsorge</h1>
                </div>
           </div>

        </div>


        <div class="container beratung">
            <div class="row">
                
                <div id="questions" class="col-sm-8">
                    
                    <tab id="1">
                        <p>1. Inflation</p>
                        <status class="checked"></status>
                    </tab>
                    
                    <tab id="2">
                        <p>2. Zinseszins</p>
                        <status class="checked"></status>
                    </tab>
                    
                    <tab id="3">
                        <p>3. Wie alt werde ich?</p>
                        <status class="checked"></status>
                    </tab>
                    
                    <tab id="4">
                        <p>4. Was ist die gesetzliche Rentenversicherung</p>
                        <status class="checked"></status>
                    </tab>
                    
                    <tab id="5">
                        <p>5. Wie lesen Sie den Rentenbescheid</p>
                        <status class="checked"></status>
                    </tab>
                    
                    <tab id="6">
                        <p>6. Wann können Sie in gesetzliche Rentenversicherung?</p>
                        <status class="checked"></status>
                    </tab>
                    
                    <tab id="7">
                        <p>7. Was bekommen Sie aus der gesetzlichen Rentenversicherung?</p>
                        <status class="checked"></status>
                    </tab>
                    
                    <tab id="8">
                        <p>8. Netto-Wunschrente</p>
                        <status></status>
                    </tab>
                    
                </div>
                
                <div class="col-sm-4">
                
                    <p>Sparbeitrag</p>
                    <slider></slider>
                    <b>00,00 €</b>
                    
                    <hr/>
                    
                    <p>Form der Altersvorsorge</p>
                    <choose>
                        <p>Blindtext</p>
                        <p>Blindtext</p>
                        <p>Blindtext</p>
                    </choose>
                    
                    <hr/>
                    
                    <p>Anlegermentalität</p>
                    <choose>
                        <p>Blindtext</p>
                        <p>Blindtext</p>
                        <p>Blindtext</p>
                    </choose>
                    
                    <hr/>
                    
                    <p>Endalter</p>
                    <slider></slider>
                    <b>00,00 €</b>
                    
                    <hr/>
                    
                    <p>Dynamik</p>
                    <choose>
                        <p>Blindtext</p>
                        <p>Blindtext</p>
                        <p>Blindtext</p>
                    </choose>
                    
                    <hr/>
                    
                    <p>Rentengarantiezeit</p>
                    <slider></slider>
                    <b>00,00 €</b>
                    
                    <a class="btn btn-white">Warenkorb speichern</a>
                    <a class="btn btn-primary">Weiter</a>
                    <a class="btn btn-secondary">Gewählte Altersvorsorge anzeigen</a>
                    <a class="btn btn-secondary">Jetzt offene Fragen klären</a>
                    
                </div>
                
                
                
                <div class="col-sm-12">

                    <div class="row">
                        <div class="col-sm-12">
                         <video id="video_player" class="vjs-big-play-centered video-js vjs-default-skin"
                          controls preload="auto" width="640" height="264"
                          poster="">
                         <source src="{{@question->get('video')}}" type='video/mp4' />
                        </video>	  
                      </div>
                      <div class="col-sm-12"> 
                      <br/>
                            <div class="row">
                               <div class="col-sm-4 text-left">
                                  <a href="/beratung/{{@question_id}}/folie">Folien download</a>
                               </div>
                               <div class="col-sm-4 text-center">
                                  <a href="/beratung/{{@question_id}}/textberatung">Textberatung</a>
                               </div>
                               <div class="col-sm-4 text-right">
                                  <a href="">Video überspringen<br/><span style="color: red; font-size: 0.5em;">Was passiert dann? *Kevin*</span></a>
                               </div>
                            </div>
                          </div>
                    </div>

                </div>

                <!--

                <div class="col-sm-6">
                   <div class="row">

                       <div class="col-sm-12">
                            <div class="row">
                               <include href="templates/standardtheme/question/forms/form.tpl" />
                            </div>
                       </div>

                   </div>
                </div>

                -->

            </div>
        </div>

        <div class="container beratung">

            <div class="row infobar">
              <div class="col-sm-12">
                 <div class="row">
                    <div class="col-sm-6 text-left">
                       <a href="#" onclick="history.back();">
                          Zurück
                       </a>
                    </div>
                    <div class="col-sm-6 text-right">
                       <a href="/beratung/getquestion">
                          Weiter
                       </a>
                    </div>
                 </div>
              </div>
            </div>

        </div>

        <script type="text/javascript">
            var player = videojs('video_player', {"fluid": true});
            player.Resume({
                uuid: '{{@question_id}}video',
                playbackOffset: 5, // begin playing video this number of seconds before it otherwise would.
                title: 'Weiterschauen',
                resumeButtonText: 'Jetzt weiterschauen',
                cancelButtonText: 'Abbrechen'
            });
        </script>
        
    </true>
    
    <false>
        
        <!--- Videoberatung -->
        <div class="container beratung">
    
           <div class="row headbar">
                <div class="col-sm-12 text-center">
                    <h1>{{@question->get('category')}}</h1>
                    <h2>{{@question->get('heading')}}</h2>
                </div>
           </div>

        </div>


        <div class="container beratung">
            <div class="row">
                <div class="col-sm-12">
                    <h3>{{@question->get('subheading')}}</h3>
                </div>
                <div class="col-sm-12">

                    <div class="row">
                        <div class="col-sm-12">
                         <video id="video_player" class="vjs-big-play-centered video-js vjs-default-skin"
                          controls preload="auto" width="640" height="264"
                          poster="">
                         <source src="{{@question->get('video')}}" type='video/mp4' />
                        </video>	  
                      </div>
                      <div class="col-sm-12"> 
                      <br/>
                            <div class="row">
                               <div class="col-sm-4 text-left">
                                  <a href="/beratung/{{@question_id}}/folie">Folien download</a>
                               </div>
                               <div class="col-sm-4 text-center">
                                  <a href="/beratung/{{@question_id}}/textberatung">Textberatung</a>
                               </div>
                               <div class="col-sm-4 text-right">
                                  <a href="">Video überspringen<br/><span style="color: red; font-size: 0.5em;">Was passiert dann? *Kevin*</span></a>
                               </div>
                            </div>
                          </div>
                    </div>

                </div>

                <!--

                <div class="col-sm-6">
                   <div class="row">

                       <div class="col-sm-12">
                            <div class="row">
                               <include href="templates/standardtheme/question/forms/form.tpl" />
                            </div>
                       </div>

                   </div>
                </div>

                -->

            </div>
        </div>

        <div class="container beratung">

            <div class="row infobar">
              <div class="col-sm-12">
                 <div class="row">
                    <div class="col-sm-6 text-left">
                       <a href="#" onclick="history.back();">
                          Zurück
                       </a>
                    </div>
                    <div class="col-sm-6 text-right">
                       <a href="/beratung/getquestion">
                          Weiter
                       </a>
                    </div>
                 </div>
              </div>
            </div>

        </div>

        <script type="text/javascript">
            var player = videojs('video_player', {"fluid": true});
            player.Resume({
                uuid: '{{@question_id}}video',
                playbackOffset: 5, // begin playing video this number of seconds before it otherwise would.
                title: 'Weiterschauen',
                resumeButtonText: 'Jetzt weiterschauen',
                cancelButtonText: 'Abbrechen'
            });
        </script>
        
    </false>

</if>

