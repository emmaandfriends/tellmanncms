<div class="container beratung">
    
   <div class="row headbar">
        <div class="col-sm-12 text-center">
            <h1>{{@question->get('heading')}}</h1>
            <h2>{{@question->get('subheading')}}</h2>
        </div>
   </div>
    
</div>
    

<div class="container-fluid beratung">
    <div class="row lightblue_content">
        <div class="col-sm-12">
           <div class="row">
               
               <div class="col-sm-12">
                    <div class="row">
                       <include href="templates/standardtheme/question/forms/form_berater.tpl" />
                    </div>
               </div>
               
           </div>
        </div>
    </div>
</div>

<div class="container beratung">
    
    <div class="row infobar">
      <div class="col-sm-12">
         <div class="row">
            <div class="col-sm-12 text-right">
               <a href="/beratung/getquestion">
                  Weiter
               </a>
            </div>
         </div>
      </div>
    </div>
    
</div>