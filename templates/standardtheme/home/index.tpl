<div class="container-fluid no-padding">
   <div class="swiper-container">
	<!-- Additional required wrapper -->
		<div class="swiper-wrapper" style="height: 850px;">
			<!-- Slides -->
			<div class="swiper-slide" style="background:url('{{ @themepath.'/images/slider.png'}}'); background-size: cover;"></div>
			<div class="swiper-slide" style="background:url('{{ @themepath.'/images/slider.png'}}'); background-size: cover;"></div>
			<div class="swiper-slide" style="background:url('{{ @themepath.'/images/slider.png'}}'); background-size: cover;"></div>
			...
		</div>
		<!-- If we need pagination -->
		<div class="swiper-pagination"></div>

		<!-- If we need navigation buttons -->
		<div class="swiper-button-prev" style="background: url(/templates/standardtheme/images/arrow_left.png); background-size: contain; background-repeat: no-repeat; background-position: center center; transform: scale(1.5);"></div>
		<div class="swiper-button-next" style="background: url(/templates/standardtheme/images/arrow_right.png); background-size: contain; background-repeat: no-repeat; background-position: center center; transform: scale(1.5);"></div>

		<!-- If we need scrollbar -->
		<div class="swiper-scrollbar"></div>
	</div>
</div>
<div class="container-fluid">
	<div class="row" style=" padding: 10px !important; ">
		<div class="col-sm-3" style="padding: 5px;">
			<img style="width: 100%;" src="{{ @themepath.'/images/Teaser 1 .png'}}">
		</div>
		<div class="col-sm-3"style="padding: 5px;">
			<img style="width: 100%;" src="{{ @themepath.'/images/Teaser 2 .png'}}">
		</div>
		<div class="col-sm-3"style="padding: 5px;">
			<img style="width: 100%;" src="{{ @themepath.'/images/Teaser 3 .png'}}">
		</div> 
		<div class="col-sm-3"style="padding: 5px;">
			<img style="width: 100%;" src="{{ @themepath.'/images/Teaser 4 .png'}}">
		</div>
	</div>
</div>


<script>
  var mySwiper = new Swiper ('.swiper-container', {
    // Optional parameters
    direction: 'horizontal',
    loop: true,
	  height: 700,
	  
    // If we need pagination
    pagination: {
      el: '.swiper-pagination',
    },
	  
	

    // Navigation arrows
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },

  })
  </script>


