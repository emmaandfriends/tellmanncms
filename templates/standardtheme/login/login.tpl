<form id="login_form" method="post" action="#">
    <div class="row">
       <div class="col-sm-6">
          <input name="password" id="password" type="password" required/>
       </div>
       <div class="col-sm-6">
          <input name="email" id="email" type="email" required/>
       </div>
        <button type="submit">Anmelden</button>
    </div>
    <input type="hidden" name="csrf" value="{{@CSRF}}" />
</form>