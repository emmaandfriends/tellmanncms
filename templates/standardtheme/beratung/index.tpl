<div class="container">
   <div class="row">
      <div class="col-sm-12 mt-4">
         <center><span class="display-4">Beratung</span><br/></center><br/>
      </div>
      <div class="col-sm-12">
            <div class="row">
               <div class="col-sm-6">
                  <a href="/beratung/starten"><div style="background: #eee; padding-top: 50px; padding-bottom: 50px;">
                     <center><p>Neue Beratung starten</p></center>
                  </div></a>
               </div>
               <div class="col-sm-6">
                  <a href="/beratung/fortsetzen"><div style="background: #eee; padding-top: 50px; padding-bottom: 50px;">
                     <center><p>Beratung fortsetzen</p></center>
                  </div></a>
               </div>
            </div>
      </div>
   </div>
</div>
