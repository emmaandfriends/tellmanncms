<?php
namespace Emma;

class BaseApp extends \Emma\Helper\Helper{
    
	public $f3;
	
	function __construct($f3){
		$this->f3 = $f3;
        $this->initDatabase();
	}
    
	public function getConfig(){
		return new \Emma\Config\Config;
	}
    
    public function initDatabase(){
        $db = new \Emma\Database\DoctrineInit($this->getConfig());
        $this->f3->set('orm', $db->getEntityManager());
    }
    
    public function isLoggedin(){
        if($this->f3->get('SESSION.email') != null && $this->f3->get('SESSION.login_string') != null){
            $user = $this->f3->get('orm')->getRepository('Emma\Models\User\User')->findOneBy(['Email' => $this->f3->get('SESSION.email')]);
            if($this->f3->get('SESSION.login_string') == hash('sha512', $user->getPassword().$_SERVER['HTTP_USER_AGENT'])){
                return true;
            }
        }
        return false;
    }
    
    public function isGuest(){
        if($this->f3->get('SESSION.email') != null){
            return false;
        }
        return true;
    }
    
    public function getUserName() {
        $username = 'Gast'; //TODO!!!!!!! -> Benutzername
        if($this->f3->get('SESSION.email') != null){
            $username = $this->f3->get('SESSION.email');
        }
        return $username;
    }
    
    public function throwMessage(int $messageType, string $message){
        $error_array = [];
        
        if($this->f3->get('ErrorMessages') != null){
            $error_array = $this->f3->get('ErrorMessages');
        }
        
        $error_array[] = ['type' => $messageType, 'msg' => $message];
        
        $this->f3->set('ErrorMessages', $error_array);
    }
    
    public function getUser(){
        $user = $this->f3->get('orm')->getRepository('Emma\Models\User\User')->findOneBy(['Email' => $this->f3->get('SESSION.email')]);
        
        if($user == null){
            return false;
        }
        
        return $user;
    }
    
    public function getUserKey(){
        if($this->f3->get('SESSION.isGuest') == true){
            if($this->f3->get('SESSION.secretGuestKey') != null){
                return $this->f3->get('SESSION.secretGuestKey');
            }
        }else{
             if($user = $this->getUser()){
                 return $user->getSecretKey();
             }
        }
        
        return false;
    }
    
    public function createGuestHash($rand_val){
        $user_hash = hash('sha512', $rand_val.$_SERVER['REMOTE_ADDR'].$_SERVER['HTTP_USER_AGENT'].date('Y.m', time())); //creates user hash
        return $user_hash;
    }
    
    public function getUserProgressID(int $position = 0){
        if($this->f3->get('SESSION.isGuest') == true){
            if($this->f3->get('SESSION.progressIdentifier') != null){
                return $this->f3->get('SESSION.progressIdentifier');
            }else{
                return false;
            }
        }else{
            if($user = $this->getUser()){
                $userProgressArray = $user->getProcesses();
                if($userProgressArray != null){
                    return $userProgressArray[$position];
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }
    }
}