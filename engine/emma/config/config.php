<?php
namespace Emma\Config;

class Config{
	private $config;
	function __construct(){
		$this->config = $this->setConfig();
	}
	
	private function setConfig(){
		$config = new \Doctrine\Common\Collections\ArrayCollection(json_decode(file_get_contents('config.json'), true));
		
		return $config;
	}
	
	public function getArrayCollection(){
		return $this->config;
	}
}