<?php
namespace Emma;

class App{
		public $f3;
		function __construct($f3){
			$this->f3 = $f3;
            $this->checkSession();
            $this->initGuest();
			$this->doRouting();
		}
		
		private function doRouting(){
			$routing = new \Emma\Routing\Routing($this->f3);
			$routing->initialize();
		}
    
        private function checkSession(){
            if($this->f3->get('SESSION.isGuest') == null){
                $this->f3->set('SESSION.isGuest', true);
            }
        }
        
        private function initGuest(){
            if($this->f3->get('SESSION.isGuest')){
                if($this->f3->get('SESSION.guestinit') == null){
                    $secret_key = hash('sha512', bin2hex($this->newKey()));
                    $rand_val = random_int(0, 19999);
                    $this->f3->set('SESSION.randval', $rand_val);
                    $this->f3->set('SESSION.progressIdentifier', $this->createGuestHash($rand_val));
                    $this->f3->set('SESSION.secretGuestKey', $secret_key);
                    $this->f3->set('SESSION.guestinit',  true);
                }
            }
        }
    
        private function newKey(){
            return openssl_random_pseudo_bytes(256);
        }
    
		public function run(){
			$this->f3->run();
		}
	
		public function createGuestHash($rand_val){
			$user_hash = hash('sha512', $rand_val.$_SERVER['REMOTE_ADDR'].$_SERVER['HTTP_USER_AGENT'].date('Y.m', time())); //creates user hash
			return $user_hash;
		}
}