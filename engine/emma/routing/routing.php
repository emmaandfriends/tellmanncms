<?php
namespace Emma\Routing;

class Routing extends \Emma\BaseApp{
	public function initialize(){
		$this->doRoutingFromArray($this->getConfig()->getArrayCollection()->get('routing'));
	}
	
	private function doRoutingFromArray($array){
		foreach($array as $route){
			$this->f3->route($route['method'].' '.$route['url'], str_replace('&#92;', '\\', $route['class']).'->createView');
		}
	}
}