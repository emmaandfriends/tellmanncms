<?php
namespace Emma\Helper;

class Helper extends \Emma\Helper\Constants{

    public static function antiXSS(string $string){
        $secure_string = preg_replace("/[^0-9]+/", "", $string);
        return $secure_string;
    }
    
    public static function generateSecretKey(){
        return openssl_random_pseudo_bytes(256);
    }
    
   
    
}