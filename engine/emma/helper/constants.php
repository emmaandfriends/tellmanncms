<?php
namespace Emma\Helper;

class Constants{
    
    const INT_SELBSTSTAENDIG = 1;
    const INT_ANGESTELLT = 2;
    const INT_BEAMTER = 3;
    
    const INT_MSG_INFO = 1;
    const INT_MSG_SUCCESS = 2;
    const INT_MSG_ERROR = 3;
    
    
}