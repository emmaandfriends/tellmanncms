<?php
namespace Emma\Controllers\Frontend\Question;

class PostDataHandler extends \Emma\Controllers\Frontend\FrontendBaseController{
    
    public function createView(){
        $progress_id = $this->f3->get("SESSION.progress_id");
        $post_data = $this->f3->get("POST");
        $step = $this->f3->get("POST.questionid");
        
        $progress = new \Emma\Models\Progress\Progress;
        $progress->savePostDatatoProgress($progress_id, $post_data, $step);
        
        $this->f3->set("SESSION.questionid", $step);
        
        $this->f3->reroute('/beratung/get');
    }
    
    
    
    
    
}