<?php
namespace Emma\Controllers\Frontend\Question;

class GetQuestionController extends \Emma\Controllers\Frontend\FrontendBaseController{
    
    public function createView(){
        $last_question = $this->f3->get('SESSION.questionid');
        if($last_question != null){
            $question = new \Emma\Models\Question\Question();
            $question = $question->getOne($last_question);
            $next_question = $this->getNextQuestion($question);
            $this->f3->reroute('/beratung/'.$next_question);
        }else{
            $this->throwMessage(self::INT_MSG_ERROR, 'Konnte letzte Frage nicht ermitteln.');
            $this->f3->set('contentfolder', 'beratung');
            $this->renderTemplate('templates/standardtheme/index.tpl');
        }
    }
    
    public function getNextQuestion(\Doctrine\Common\Collections\ArrayCollection $question){
        $question_array = $question->get('nextQuestion');
        $next_question_id = false;
        
        foreach($question_array as $nextQuestion){
            
            if(isset($nextQuestion['condition'])){
                $condition = $nextQuestion['condition'];
                
                switch($condition['type']){
                    case 'if':
                        $progress_entity = new \Emma\Models\Progress\Progress();
                        $progress_entity = $progress_entity->getOne($this->getUserProgressID(), $this->getUserKey());
                        $test_var = $progress_entity->get($condition['var']);
                        if($test_var == $condition['is']){
                            $next_question_id = $nextQuestion['questionID']; 
                        }
                        break;
                    default:
                        $next_question_id = false;  
                }
                
            }else{
                $next_question_id = $nextQuestion['questionID'];
            }
            
        }
        
        return $next_question_id;
    }
}