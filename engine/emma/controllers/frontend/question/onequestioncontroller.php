<?php
namespace Emma\Controllers\Frontend\Question;

class OneQuestionController extends \Emma\Controllers\Frontend\FrontendBaseController{
    
    private $textberatung;
    private $question;
    
    public function __construct($f3, $args){
        $this->textberatung = false;
        parent::__construct($f3, $args);
    }

    public function createView(){
        $this->f3->set('contentfolder', 'question');
        $question_id = $this->args['questionid'];
        
        
        if(isset($this->args['additional_param'])){
            switch($this->args['additional_param']){
                case 'textberatung':
                    $this->textberatung = true;
                case 'video':
                    $this->downloadPDF();
            }
        }

        $question_model = new \Emma\Models\Question\Question;
        $this->question = $question_model->getOne($question_id);

        $this->f3->set('question_id', $question_id);
        $this->f3->set('question', $this->question);
        $this->f3->set('textberatung', $this->textberatung);
        $this->f3->set('SESSION.questionid', $question_id);
        
        if($this->question->get('force_template') != null){
            $this->f3->set('contentfolder', 'question/'.$this->question->get('force_template'));
        }
        
        $this->renderForms();

        $this->renderTemplate('templates/standardtheme/index.tpl');
        
    }

    private function renderForms(){
        if($this->question->containsKey('forms')){
            $this->f3->set('hasforms', true);
            $forms = [];
            
            foreach($this->question->get('forms') as $form_id){
                $form = new \Emma\Models\Question\Form;
                $forms[] = $form->getOne($form_id)->toArray();
            }
            
            $this->f3->set('forms', $forms);
        }
    }
    
    private function downloadPDF(){
        
        
        
    }
    
}