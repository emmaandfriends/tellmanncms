<?php
namespace Emma\Controllers\Frontend\Question;

class QuestionController extends \Emma\Controllers\Frontend\FrontendBaseController{
    
    private $textberatung;
    private $question;
    
    public function __construct($f3, $args){
        $this->textberatung = false;
        parent::__construct($f3, $args);
    }

    public function createView(){
        $this->f3->set('contentfolder', 'question');
        $question_id = $this->args['questionid'];
        
        if($question_id == 'start'){
            $controller = new \Emma\Controllers\Frontend\Beratung\StartController($this->f3, $this->$args);
            $controller->createView();
        }elseif($question_id == 'get'){
            $this->questStarten();
        }elseif($question_id == 'starten'){
            $this->questStarten();
        }elseif($question_id == 'fortsetzen'){
            $this->questContinue();
        }else{
            if(isset($this->args['additional_param'])){
                switch($this->args['additional_param']){
                    case 'textberatung':
                        $this->textberatung = true;
                    case 'video':
                        $this->downloadPDF();
                }
            }

            $question_model = new \Emma\Models\Question\Question;
            $this->question = $question_model->getOne($question_id);

            $this->f3->set('question_id', $question_id);
            $this->f3->set('question', $this->question);
            $this->f3->set('textberatung', $this->textberatung);

            $this->renderForms();

            $this->renderTemplate('templates/standardtheme/index.tpl');
        }
    }
    
    private function questStart(){
        if($this->isGuest()){
            $this->renderJobChoice();
        }elseif($this->isLoggedin()){
            
            
            
        }else{
            $guest = new \Emma\Models\Guest\Guest();
            $this->questStart();
        }
    }
    
    private function renderJobChoice(){
        $this->f3->set('contentfolder', 'jobwahl');
        $this->renderTemplate('templates/standardtheme/index.tpl');
    }
    
    private function questContinue(){
        
        
        
    }
    
    private function renderForms(){
        if($this->question->containsKey('forms')){
            $this->f3->set('hasforms', true);
            $forms = [];
            
            foreach($this->question->get('forms') as $form_id){
                $form = new \Emma\Models\Question\Form;
                $forms[] = $form->getOne($form_id)->toArray();
            }
            
            $this->f3->set('forms', $forms);
        }
    }
    
    private function downloadPDF(){
        
        
        
    }
    
}