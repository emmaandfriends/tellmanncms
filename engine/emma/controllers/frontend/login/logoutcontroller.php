<?php
namespace Emma\Controllers\Frontend\Login;

class LogoutController extends \Emma\Controllers\Frontend\FrontendBaseController{
    
    public function __construct($f3, $args){
        parent::__construct($f3, $args);
    }

    public function createView(){
        $this->logoutUser();
        
        $this->f3->reroute('/');
    }
    
    private function logoutUser(){
        $logoutHandler = new \Emma\Components\Login\LoginHandler($this->f3);
        $logoutHandler->logoutUser();
    }
}