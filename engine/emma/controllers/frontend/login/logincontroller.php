<?php
namespace Emma\Controllers\Frontend\Login;

class LoginController extends \Emma\Controllers\Frontend\FrontendBaseController{
    
    public function __construct($f3, $args){
        parent::__construct($f3, $args);
    }

    public function createView(){
        if($this->f3->get('POST.password') != null && $this->f3->get('POST.email') != null){
            $this->loginUser();
        }
        
        $this->f3->set('contentfolder', 'login');
        $this->f3->set('renderLogin', true);

        $this->renderTemplate('templates/standardtheme/index.tpl');
    }
    
    private function loginUser(){
        $loginHandler = new \Emma\Components\Login\LoginHandler($this->f3);
        $logged_in = $loginHandler->loginUser($this->f3->get('POST.password'), $this->f3->get('POST.email'));
        
        if($logged_in){
            $this->throwMessage(self::INT_MSG_SUCCESS, 'Sie wurden erfolgreich eingeloggt');
        }
    }
}