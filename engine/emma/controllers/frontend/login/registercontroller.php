<?php
namespace Emma\Controllers\Frontend\Login;

class RegisterController extends \Emma\Controllers\Frontend\FrontendBaseController{
    
    public function __construct($f3, $args){
        parent::__construct($f3, $args);
    }

    public function createView(){
        if($this->f3->get('POST.password') != null && $this->f3->get('POST.email') != null){
            $this->registerUser();
        }
        
        $this->f3->set('contentfolder', 'login');
        $this->f3->set('renderRegister', true);
        
        $this->renderTemplate('templates/standardtheme/index.tpl');
    }
    
    private function registerUser(){
        $registerHandler = new \Emma\Components\Login\LoginHandler($this->f3);
        $registered = $registerHandler->registerUser($this->f3->get('POST.password'), $this->f3->get('POST.email'));
        
        if($registered){
            $this->throwMessage(self::INT_MSG_SUCCESS, 'Sie haben erfolgreich ein Benutzerkonto angelegt.');
        }    
    }
}