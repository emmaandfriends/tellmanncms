<?php
namespace Emma\Controllers\Frontend\Api;

class ApiController extends \Emma\Controllers\Frontend\FrontendBaseController{

    public function createView(){
        if(isset($this->args['model'])){
            switch($this->args['model']){
                case 'question':
                    $this->doQuestion();
                    break;
                default:
                    $this->apiError('Model not known');
            }
        }else{
            $this->apiError('No Model given or null');
        }
    }

    private function doQuestion(){
        if(isset($this->args['method'])){
            switch($this->args['method']){
                case 'get':
                    if(isset($this->args['objectid'])){
                        $question = new \Emma\Models\Question\Question;
                        $this->apiSuccess($question->getOne($this->args['objectid'])->toArray());
                    }else{
                        $this->apiError('No Object ID given or null');
                    }
                    break;
                case 'getNext':
                    $last_question = $this->f3->get('SESSION.questionid');
                    $question_id = 1;
                    if($last_question != null){
                        $question = new \Emma\Models\Question\Question();
                        $question = $question->getOne($last_question);
                        $getquestioncontroller = new \Emma\Controllers\Frontend\Question\GetQuestionController($this->f3, $this->args);
                        $question_id = $getquestioncontroller->getNextQuestion($question);
                        if($question_id == false){
                            $this->apiError('Next Question Paramater missing in question');
                        }else{
                            $question = new \Emma\Models\Question\Question;
                            $this->apiSuccess($question->getOne($question_id)->toArray());
                        }
                    }
                    break;
                default:
                    $this->apiError('Method not known');
            }
        }else{
            $this->apiError('No Method given or null');
        }
    }
    
    private function apiError($message){
        $request = ['model' => $this->args['model'], 'method' => $this->args['method'], 'objectid' => $this->args['objectid']];
        $message = ['type' => 'error', 'message' => $message, 'request' => $request];
        echo json_encode($message);
    }
    
    private function apiSuccess($array){
        $request = ['model' => $this->args['model'], 'method' => $this->args['method'], 'objectid' => $this->args['objectid']];
        $message = ['type' => 'success', 'data' => $array, 'request' => $request];
        echo json_encode($message);
    }
}