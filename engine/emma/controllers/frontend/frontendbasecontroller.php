<?php
namespace Emma\Controllers\Frontend;

class FrontendBaseController extends \Emma\BaseApp{
		public $f3, $args;
		function __construct($f3, $args){
			$this->f3 = $f3;
			$this->args = $args;
            $this->f3->set('themepath', '/templates/'.$this->getConfig()->getArrayCollection()->get('template'));
            $username = $this->getUserName();
            $this->f3->set('username', $username);
		}
		
		public function renderView($template){
			echo View::instance()->render($template);
		}
    
        public function renderTemplate($template){
            echo \Template::instance()->render($template);
        }
    
        public function createView(){
            
        }
    
}