<?php
namespace Emma\Controllers\Frontend\Beratung;

class JobChoiceController extends \Emma\Controllers\Frontend\FrontendBaseController{
    
    public function createView(){
        if($this->f3->get('jobparameter') != null){
            switch($this->f3->get('jobparameter')){
                case 'selbststaendig':
                    $this->f3->set('SESSION.jobtype', self::INT_SELBSTSTAENDIG);
                    break;
                case 'angestellt':
                    $this->f3->set('SESSION.jobtype', self::INT_ANGESTELLT);
                    break;
                case 'beamter':
                    $this->f3->set('SESSION.jobtype', self::INT_BEAMTER);
            }
        }
        
        $this->f3->reroute('/question/get');
    }
    
}