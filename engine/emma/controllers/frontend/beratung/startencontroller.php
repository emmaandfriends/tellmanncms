<?php
namespace Emma\Controllers\Frontend\Beratung;

class StartenController extends \Emma\Controllers\Frontend\FrontendBaseController{
    
    public function createView(){
        $this->f3->set('contentfolder', 'beratung');
        //Abfrage Berufsstand
        $this->renderTemplate('templates/standardtheme/index.tpl');
    }
    
    
}