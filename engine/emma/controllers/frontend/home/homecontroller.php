<?php
namespace Emma\Controllers\Frontend\Home;

class HomeController extends \Emma\Controllers\Frontend\FrontendBaseController{
    
        public function createView(){
            $this->f3->set('contentfolder', 'home');
            $this->renderTemplate('templates/standardtheme/index.tpl');
            $question = new \Emma\Models\Question\Question;
            $question->persist(['test' => '1234']);
        }
    
}