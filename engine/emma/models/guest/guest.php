<?php
namespace Emma\Models\Guest;

class Guest{
    
    public function get(){
        return $this->getAll();
    }
    
    public function getAll(){
        $files = [];
        foreach (glob("storage/guests/*.json") as $file) {
          $files[] = json_decode(file_get_contents($file), true);
        }
        return new \Doctrine\Common\Collections\ArrayCollection($files);
    }
    
    public function getOne(string $identifier){
        $file = json_decode(file_get_contents('storage/guests/'.$identifier.'.json'), true);
        
        return new \Doctrine\Common\Collections\ArrayCollection($file);
    }
    
    public function getMe(int $rand_val){
        $identifier = $this->createGuestHash($rand_val);
        return $this->getOne($identifier);
    }
    
    public function persist(int $rand_val, $data){
        $identifier = $this->getnewId($rand_val);
        $this->update($identifier, $data);
    }
    
    public function update($identifier, $data){
        file_put_contents('storage/guests/'.$identifier.'.json', json_encode($data));
    }
    
    public function getnewId($rand_val){
        return $this->createGuestHash($rand_val);
    }
    
    private function createGuestHash($rand_val){
        $user_hash = hash('sha512', $rand_val.$_SERVER['REMOTE_ADDR'].$_SERVER['HTTP_USER_AGENT'].date('Y.m', time())); //creates user hash
        return $user_hash;
    }
    
    
    
}