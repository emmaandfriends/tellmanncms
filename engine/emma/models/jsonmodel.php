<?php
namespace Emma\Models;

class JsonModel{
    
    public function get(){
        return $this->getAll();
    }
    
    public function getAll(){
        $files = [];
        foreach (glob("storage/questions*.json") as $file) {
          $files[] = json_decode(file_get_contents($file), true);
        }
        return new \Doctrine\Common\Collections\ArrayCollection($files);
    }
    
    public function getOne($identifier){
        $file = json_decode(file_get_contents($identifier.'.json'), true);
        
        return new \Doctrine\Common\Collections\ArrayCollection($file);
    }
    
    public function persist($data){
        $identifier = $this->getnewId();
        $this->update($identifier, $data);
    }
    
    public function update($identifier, $data){
        file_put_contents($identifier.'.json', json_encode($data));
    }
    
    public function getnewId(){
        $id = 0;
        foreach (glob("storage/questions*.json") as $file) {
          $id += 1;
        }
        
        return $id + 1;
    }
    
}