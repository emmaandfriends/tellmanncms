<?php
namespace Emma\Models\User;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity(repositoryClass="\Emma\Models\Repositories\User\User")
 * @Table(name="user")
 */
class User
{
    /**
     * @Id
     * @Column(type="integer", name="id")
     * @GeneratedValue
     */
    protected $Id;
    
    /**
     * @Column(name="email")
     */
    protected $Email;
    
    /**
     * @OneToMany(targetEntity="Adress", mappedBy="user")
     */
    protected $Addresses;
    
    /**
     * @Column(type="text", name="password")
     */
    private $Password;
    
    /**
     * @Column(type="string", name="password_salt")
     */
    private $PasswordSalt;
    
    /**
     * @Column(type="string", name="secret_key")
     */
    protected $SecretKey;
    
    /**
     * @Column(type="array", name="processes", nullable=true)
     */
    private $Processes;
    
    /**
     * @OneToMany(targetEntity="Contract", mappedBy="user")
     */
    private $Contracts;
    
    public function __construct() {
        $this->Addresses = new ArrayCollection();
        $this->Contracts = new ArrayCollection();
    }
    
    public function getId(){
        return $this->Id;
    }
    
    public function getEmail(){
        return $this->Email;
    }
    
    public function getAdresses(){
        return $this->Adresses;
    }
    
    public function getPassword(){
        return $this->Password;
    }
    
    public function getPasswordSalt(){
        return $this->PasswordSalt;
    }
    
    public function getSecretKey(){
        return $this->SeretKey;
    }
    
    public function getProcesses(){
        return $this->Processes;
    }
    
    public function getContracts(){
        return $this->Contracts;
    }
    
     public function setEmail($Email){
        $this->Email = $Email;
    }
    
    public function setPassword($Password){
        $this->Password = $Password;
    }
    
    public function setPasswordSalt($PasswordSalt){
        $this->PasswordSalt = $PasswordSalt;
    }
    
    public function setSecretKey($SecretKey){
        $this->SecretKey = $SecretKey;
    }
    
     public function setProcesses($Processes){
        $this->Processes = $Processes;
    }
    
}