<?php
namespace Emma\Models\User;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * @Entity(repositoryClass="\Emma\Models\Repositories\User\Adress")
 * @Table(name="adress")
 */
class Adress
{
    /**
     * @Id
     * @Column(type="integer", name="id")
     * @GeneratedValue
     */
    protected $Id;
    
    /**
     * @Column(name="surname")
     */
    protected $Surname;
    
    /**
     * @Column(name="firstname")
     */
    protected $Firstname;
    
    /**
     * @Column(name="street")
     */
    protected $Street;
    
    /**
     * @Column(name="house_number")
     */
    protected $HouseNumber;
    
    /**
     * @Column(name="zip_code")
     */
    protected $ZipCode;
    
    /**
     * @Column(name="city")
     */
    protected $City;
    
    /**
     * @ManyToOne(targetEntity="User", inversedBy="addresses")
     * @JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $User;
    
    public function getId(){
        return $this->Id;
    }
    
    public function getSurname(){
        return $this->Surname;
    }
    
    public function getFirstname(){
        return $this->Firstname;
    }
    
    public function getStreet(){
        return $this->Street;
    }
    
    public function getHouseNumber(){
        return $this->HouseNumber;
    }
    
    public function getZipCode(){
        return $this->ZipCode;
    }
    
    public function getCity(){
        return $this->City;
    }
    
    public function getUser(){
        return $this->User;
    }
    
    public function setSurname($Surname){
        $this->Surname = $Surname;
    }
    
    public function setFirstname($Firstname){
        $this->Firstname = $Firstname;
    }
    
    public function setStreet($Street){
        $this->Street = $Street;
    }
    
    public function setHouseNumber($HouseNumber){
        $this->HouseNumber = $HouseNumber;
    }
    
    public function setZipCode($ZipCode){
         $this->ZipCode = $ZipCode;
    }
    
    public function setCity($City){
        $this->City = $City;
    }
    
    public function setUser($User){
        $this->User = $User;
    }
}