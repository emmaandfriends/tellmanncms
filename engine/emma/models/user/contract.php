<?php
namespace Emma\Models\User;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * @Entity(repositoryClass="\Emma\Models\Repositories\User\Contract")
 * @Table(name="contract")
 */
class Contract
{
    /**
     * @Id
     * @Column(type="integer", name="id")
     * @GeneratedValue
     */
    protected $Id;
    
    /**
     * @Column(name="surname")
     */
    protected $TestString;
    
    /**
     * @ManyToOne(targetEntity="User", inversedBy="contracts")
     * @JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $User;
    
    public function getId(){
        return $this->Id;
    }
    
    public function getTestString(){
        return $this->TestString;
    }
    
    public function getUser(){
        return $this->User;
    }
    
    public function setTestString($TestString){
        $this->TestString = $TestString;
    }
    
    public function setUser($User){
        $this->User = $User;
    }
    
    public function getHouseNumber(){
        return $this->HouseNumber;
    }
}