<?php
namespace Emma\Models\Session;

/**
 * @Entity
 * @Table(name="session")
 */
class Session
{
    /**
     * @Id
     * @Column(type="string", name="id", length=128, options={"fixed" = true})
     */
    protected $Id;
    
    /**
     * @Column(type="string", name="set_time", length=10, options={"fixed" = true})
     */
    protected $setTime;
    
    /**
     * @Column(type="text", name="data")
     */
    protected $data;
    
    /**
     * @Column(type="string", name="session_key", length=128, options={"fixed" = true})
     */
    protected $sessionKey;
    
    public function getId(){
        return $this->Id;
    }
    
    public function getSetTime(){
        return $this->setTime;
    }
    
    public function getData(){
        return $this->data;
    }
    
    public function getSessionKey(){
        return $this->sessionKey;
    }
    
    public function setId($Id){
        $this->Id = $Id;
    }
    
    public function setSetTime($SetTime){
        $this->setTime = $SetTime;
    }
    
    public function setData($Data){
        $this->data = $Data;
    }
    
    public function setSessionKey($SessionKey){
        $this->sessionKey = $SessionKey;
    }
    
    
}