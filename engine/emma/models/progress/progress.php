<?php
namespace Emma\Models\Progress;

class Progress{
    
    public function getAll(){
        $files = [];
        foreach (glob("storage/progress/*.json") as $file) {
          $files[] = json_decode(file_get_contents($file), true);
        }
        return new \Doctrine\Common\Collections\ArrayCollection($files);
    }
    
    public function getOne(string $identifier, string $key){
        $file = file_get_contents('storage/progress/'.$identifier.'.json');
        
        $file = json_decode($this->encrypt_decrypt('decrypt', $file, $key), true);
        
        if($file == null || $file == false){
            return false;
        }
        
        return new \Doctrine\Common\Collections\ArrayCollection($file);
    }
    
    public function persist($identifier, $key, $data){
        $this->update($identifier, $key, $data);
    }
    
    public function update($identifier, $key, $data){
        $save_string = $this->encrypt_decrypt('encrypt', json_encode($data), $key);
        
        file_put_contents('storage/progress/'.$identifier.'.json', $save_string);
    }
    
    public function savePostDatatoProgress(string $progress_id, array $post_data, string $key, string $step){
        $progress_obj = [];
        $update = false;
        if($progress_obj = $this->getOne($progress_id, $key)){
            $progress_obj = $progress_obj->toArray();
            $update = true;
        }
        
        foreach($post_data as $post_key => $post_element){
            $progress_obj['steps'][$step][$post_key] = $post_element;
        }
        
        $this->update($progress_id, $key, $progress_obj);
    }
    
    private function encrypt_decrypt($action, $string, $key){
        $output = false;
        $encrypt_method = "AES-256-CBC";
        $secret_key = $key;
        $secret_iv = 'f14b46373cd84072854d197de9056ae2';

        $key = hash('sha256', $secret_key);
        
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        
        if ($action == 'encrypt') {
            $output = base64_encode(openssl_encrypt($string, $encrypt_method, $key, 0, $iv));
        } else {
            if ($action == 'decrypt') {
                $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
            }
        }
        
        return $output;
    }
}