<?php
namespace Emma\Components\Session;

class EmmaSession {
    
    function __construct(){
        session_set_save_handler(array($this, 'open'), array($this, 'close'), array($this, 'read'), array($this, 'write'), array($this, 'destroy'), array($this, 'gc'));
        register_shutdown_function('session_write_close');
    }
    
    function start_session($session_name, $secure) {
        $httponly = true;
        $session_hash = 'sha512';

        if (in_array($session_hash, hash_algos())) {
            ini_set('session.hash_function', $session_hash);
        }
        
        ini_set('session.hash_bits_per_character', 5);

        ini_set('session.use_only_cookies', 1);

        $cookieParams = session_get_cookie_params(); 
        
        session_set_cookie_params($cookieParams["lifetime"], $cookieParams["path"], $cookieParams["domain"], $secure, $httponly); 
        
        session_name($session_name);
        
        session_start();
        
        session_regenerate_id(true); 
    }
    
    function open() {
        $config = new \Emma\Config\Config();
        $doctrine = new \Emma\Database\DoctrineInit($config);
        $this->db = $doctrine->getEntityManager();
        return true;
    }
    
    function close() {
        $this->db->flush();
        $this->db->clear();
        $this->db->close();
        return true;
    }
    
    function read($id) {
        $data = $this->db->getRepository('Emma\Models\Session\Session')->find($id);
        if($data != null){
            $data = $data->getData();
            
            $key = $this->getkey($id);
            
            $data = $this->encrypt_decrypt('decrypt', $data, $key);
    
        }else{
            $data = '';
        }
            
        return $data;
    }
    
    function destroy($id) {
        $entity = $this->db->getRepository('Emma\Models\Session\Session')->find($id);
        if($entity != null){
            $this->db->remove($entity);
        }
        
        return true;
    }
    
    function gc($max) {
        $entityRepository = $this->db->getRepository('Emma\Models\Session\Session');
        
        $old = time() - $max;
        $criteria = new \Doctrine\Common\Collections\Criteria();
        $criteria->where($criteria::expr()->lt('setTime', $old));

        $result = $entityRepository->matching($criteria);
        $result = $result->toArray();
        
        foreach($result as $entity){
            $this->db->remove($entity);
        }
        
        return true;
    }
    
    private function getkey($id) {
        $session = $this->db->getRepository('Emma\Models\Session\Session')->find($id);
        
        if($session != null){
            return $session->getSessionKey();
        }else{
            $random_key = hash('sha512', uniqid(bin2hex(openssl_random_pseudo_bytes(256)), true));
            return $random_key;
        }
    }
    
    function write($id, $data) {
        $key = $this->getkey($id);
        $data = $this->encrypt_decrypt('encrypt', $data, $key);
        $persist = false;

        $time = time();
        $session = $this->db->getRepository('Emma\Models\Session\Session')->find($id);
        if($session == null){
            $session = new \Emma\Models\Session\Session();
            $persist = true;
        }
        
        $session->setId($id);
        $session->setSetTime($time);
        $session->setData($data);
        $session->setSessionKey($key);
        
        
        if($persist){
            $this->db->persist($session);
        }
        
        return true;
    }
    
    function encrypt_decrypt($action, $string, $key)
    {
        $output = false;
        $encrypt_method = "AES-256-CBC";
        $secret_key = $key;
        $secret_iv = 'f24b47894cd74073854b197df9056ee9';

        $key = hash('sha256', $secret_key);
        
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        
        if ($action == 'encrypt') {
            $output = base64_encode(openssl_encrypt($string, $encrypt_method, $key, 0, $iv));
        } else {
            if ($action == 'decrypt') {
                $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
            }
        }
        
        return $output;
    }
    
}