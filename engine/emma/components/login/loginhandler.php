<?php
namespace Emma\Components\Login;

class LoginHandler extends \Emma\BaseApp{
    
    public function loginUser($password, $email){
        $user = $this->f3->get('orm')->getRepository('Emma\Models\User\User')->findOneBy(['Email' => $email]);
        if($user != null){
            $password_hash = hash('sha512', $password . $user->getPasswordSalt());
            if($user->getPassword() == $password_hash){
                $this->f3->set('SESSION.email', $email);
                $this->f3->set('SESSION.isGuest', false);
                $this->f3->set('SESSION.login_string', hash('sha512', $user->getPassword().$_SERVER['HTTP_USER_AGENT']));
                echo "ttteeee";
                return true;
            }
        }
        
        $this->throwMessage(self::INT_MSG_ERROR, 'Kombination aus Passwort und E-Mail Adresse nicht gültig.');
        
        return false;
    }
    
    public function logoutUser(){
        $this->f3->clear('SESSION');
    }
    
    public function registerUser($password, $email){
        $em = $this->f3->get('orm');
        
        $password = filter_var($password, FILTER_SANITIZE_STRING);
        $email = filter_var($email, FILTER_SANITIZE_STRING);
        
        $salt = $this->generateSalt();
        $password_hash = hash('sha512', $password . $salt);
        
        $user = $em->getRepository('Emma\Models\User\User')->findOneBy(['Email' => $email]);
        if($user != null){
            $this->throwMessage(self::INT_MSG_ERROR, 'Es existiert bereits ein Benutzer mit der angegebenen E-Mail Adresse.');
            return false;
        }
        
        $secret_key = bin2hex($this->generateSecretKey());
            
        $user = new \Emma\Models\User\User();
        $user->setEmail($email);
        $user->setPassword($password_hash);
        $user->setPasswordSalt($salt);
        $user->setSecretKey($secret_key);
        
        $em->persist($user);
        $em->flush();
        
        return true;
    }
    
    private function generateSalt(){
        return hash('sha512', uniqid(openssl_random_pseudo_bytes(16), TRUE));
    }

}