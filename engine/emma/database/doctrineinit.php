<?php
namespace Emma\Database;

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

class DoctrineInit{
    
    private $entityManager;
    private $config;
    function __construct($config){
        $this->config = $config;
        $this->initEntityManager();
    }
    
    public function getEntityManager(){
        return $this->entityManager;
    }
    
    private function setEntityManager($em){
        $this->entityManager = $em;
    }
    
    private function initEntityManager(){
        $db_config = $this->config->getArrayCollection()->get('db');
        $entity_directories = ["engine/emma/models/user", "engine/emma/models/session"];
        
        $config = Setup::createAnnotationMetadataConfiguration($entity_directories, true);
        $entityManager = EntityManager::create($db_config, $config);
        
        $this->setEntityManager($entityManager);
    }
    
}
