<?php
// bootstrap.php
require_once "vendor/autoload.php";
$f3 = require('vendor/bcosca/fatfree-core/base.php');
$f3->set('DEBUG', 3);

//Load App
$f3->set('AUTOLOAD','engine/');

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

$paths = array("engine/emma/models/user");
$isDevMode = true;

// the connection configuration
$dbParams = array(
    'driver'   => 'pdo_mysql',
    'user'     => 'tellmann',
    'password' => '1017Emma&Friends!',
    'dbname'   => 'tellmann',
);

$config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode);
$entityManager = EntityManager::create($dbParams, $config);

$tool = new \Doctrine\ORM\Tools\SchemaTool($entityManager);
$classes = array(
  $entityManager->getClassMetadata('Emma\Models\User\User'),
    $entityManager->getClassMetadata('Emma\Models\User\Adress'),
    $entityManager->getClassMetadata('Emma\Models\User\Contract'),
    $entityManager->getClassMetadata('Emma\Models\Session\Session'),
);

$tool->updateSchema($classes);