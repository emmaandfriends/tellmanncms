<?php
//Require external libs
require_once('vendor/autoload.php');

//Kickstart F3
$f3 = require('vendor/bcosca/fatfree-core/base.php');
$f3->set('DEBUG', 3);

//Autoload Engine
$f3->set('AUTOLOAD','engine/');
$f3->set('CACHE', 'folder=tmp/cache');

//Load and start EmmaSession
$session = new \Emma\Components\Session\EmmaSession();
$session->start_session('_s', false);

//Run EmmaApp on top of F3
$app = new \Emma\App($f3);
$app->run();